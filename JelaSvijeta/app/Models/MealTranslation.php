<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MealTranslation extends Model
{
    protected $fillable = ['meal_id', 'locale', 'title', 'description'];

    public function meal()
    {
        return $this->belongsTo(Meal::class);
    }
}