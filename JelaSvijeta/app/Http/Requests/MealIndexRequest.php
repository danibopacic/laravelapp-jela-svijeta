<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MealIndexRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'per_page'    => 'integer|min:1',
            'page'        => 'integer|min:1',
            'category'    => 'nullable|integer|exists:categories,id',
            'tags'        => 'array',
            'tags.*'      => 'integer|exists:tags,id',
            'with'        => 'array',
            'with.*'      => 'in:ingredients,category,tags',
            'lang'        => 'required|string|exists:languages,code',
            'diff_time'   => 'integer|min:0',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'per_page.integer' => 'Parametar "per_page" mora biti broj.',
            'per_page.min' => 'Parametar "per_page" mora biti najmanje 1.',
            'page.integer' => 'Parametar "page" mora biti broj.',
            'page.min' => 'Parametar "page" mora biti najmanje 1.',
            'category.integer' => 'Parametar "category" mora biti broj.',
            'category.exists' => 'Odabrana kategorija ne postoji.',
            'tags.array' => 'Parametar "tags" mora biti niz.',
            'tags.*.integer' => 'Svaki tag mora biti broj.',
            'tags.*.exists' => 'Odabrani tag ne postoji.',
            'with.array' => 'Parametar "with" mora biti niz.',
            'with.*.in' => 'Neispravan zahtjev za učitavanje podataka.',
            'lang.required' => 'Jezik je obavezan parametar.',
            'lang.string' => 'Jezik mora biti tekst.',
            'lang.exists' => 'Odabrani jezik ne postoji.',
            'diff_time.integer' => 'Parametar "diff_time" mora biti broj.',
            'diff_time.min' => 'Parametar "diff_time" ne može biti manji od 0.',
        ];
    }
}