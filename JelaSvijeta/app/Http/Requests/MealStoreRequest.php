<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MealStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'category_id' => 'nullable|integer|exists:categories,id',
            'tags' => 'nullable|array',
            'tags.*' => 'integer|exists:tags,id',
            'ingredients' => 'nullable|array',
            'ingredients.*' => 'integer|exists:ingredients,id',
            'lang' => 'required|string|in:en,hr,...',
        ];
    }
}
