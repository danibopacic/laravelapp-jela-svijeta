<?php

namespace App\Http\Controllers;

use App\Models\Meal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\MealIndexRequest;
use App\Http\Requests\MealStoreRequest;

class MealController extends Controller
{
    public function index(MealIndexRequest $request)
    {
        $validated = $request->validated();

        $lang = $validated['lang'];
        $query = Meal::withTranslation($lang);

        if (isset($validated['category'])) {
            if ($validated['category'] === 'NULL') {
                $query->whereNull('category_id');
            } elseif ($validated['category'] === '!NULL') {
                $query->whereNotNull('category_id');
            } else {
                $query->where('category_id', $validated['category']);
            }
        }


        if (isset($validated['tags'])) {
            $query->whereHas('tags', function ($q) use ($validated) {
                $q->whereIn('tags.id', $validated['tags']);
            }, '=', count($validated['tags']));
        }


        if (isset($validated['diff_time']) && $validated['diff_time'] > 0) {
            $timestamp = date('Y-m-d H:i:s', $validated['diff_time']);
            $query->withTrashed()
                  ->where(function ($q) use ($timestamp) {
                      $q->where('created_at', '>', $timestamp)
                        ->orWhere('updated_at', '>', $timestamp)
                        ->orWhere('deleted_at', '>', $timestamp);
                  });
        }


        if (isset($validated['with'])) {
            $query->with($validated['with']);
        }


        $perPage = $validated['per_page'] ?? 15;
        $page = $validated['page'] ?? 1;

        $meals = $query->paginate($perPage, ['*'], 'page', $page);

        $response = [
            'meta' => [
                'currentPage'   => $meals->currentPage(),
                'totalItems'    => $meals->total(),
                'itemsPerPage'  => $meals->perPage(),
                'totalPages'    => $meals->lastPage(),
            ],
            'data' => $meals->items(),
            'links' => [
                'prev' => $meals->previousPageUrl(),
                'next' => $meals->nextPageUrl(),
                'self' => $meals->url($meals->currentPage()),
            ],
        ];

        return response()->json($response);
    }

    public function store(MealStoreRequest $request)
    {
        $validated = $request->validated();

        $meal = new Meal();
        $meal->category_id = $validated['category_id'] ?? null;
        $meal->save();

        if (isset($validated['tags'])) {
            $meal->tags()->sync($validated['tags']);
        }

        if (isset($validated['ingredients'])) {
            $meal->ingredients()->sync($validated['ingredients']);
        }

        $meal->translations()->create([
            'locale' => $validated['lang'],
            'title' => $validated['name'],
            'description' => $validated['description']
        ]);

        return response()->json(['message' => 'Meal created successfully', 'meal' => $meal], 201);
    }
}
