<?php

use Illuminate\Http\Request;

use App\Http\Controllers\MealController;

Route::get('/meals', [MealController::class, 'index']);
Route::post('/meals', [MealController::class, 'store']);