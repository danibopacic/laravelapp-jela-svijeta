<?php

use Illuminate\Database\Seeder;
use App\Models\Meal;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Tag;
use Carbon\Carbon;
use Faker\Factory as Faker;

class MealSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now();

        $categories = Category::all();
        $ingredients = Ingredient::all();
        $tags = Tag::all();

        // Create 10 meals
        for ($i = 0; $i < 10; $i++) {

            $category = $categories->random();
            $categoryId = $i % 2 === 0 ? $category->id : null;


            $meal = Meal::create([
                'category_id' => $categoryId,
                'created_at' => $now,
                'updated_at' => $now,
                'deleted_at' => null,
            ]);


            foreach (['en', 'hr'] as $locale) {
                $meal->translateOrNew($locale)->title = $faker->sentence;
                $meal->translateOrNew($locale)->description = $faker->paragraph;
            }
            $meal->save();

            // Attach random ingredients
            $meal->ingredients()->attach(
                $ingredients->random(rand(1, 3))->pluck('id')->toArray(),
                ['created_at' => $now, 'updated_at' => $now]
            );

            // Attach random tags
            $meal->tags()->attach(
                $tags->random(rand(1, 3))->pluck('id')->toArray(),
                ['created_at' => $now, 'updated_at' => $now]
            );
        }
    }
}