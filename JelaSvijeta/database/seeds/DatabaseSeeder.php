<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            LanguageSeeder::class,
            CategorySeeder::class,
            IngredientSeeder::class,
            TagSeeder::class,
            MealSeeder::class,
        ]);
    }
}
