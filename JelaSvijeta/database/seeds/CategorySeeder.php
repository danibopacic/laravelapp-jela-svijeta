<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use Faker\Factory as Faker;
use Carbon\Carbon;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now();

        for ($i = 0; $i < 10; $i++) {

            $category = Category::create([
                'slug' => $faker->slug,
                'created_at' => $now,
                'updated_at' => $now,
                'deleted_at' => null
            ]);

            foreach (['en', 'hr'] as $locale) {
                $category->translateOrNew($locale)->title = $faker->word;
            }
            
            $category->save();
        }
    }
}