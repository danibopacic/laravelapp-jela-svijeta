<?php

use Illuminate\Database\Seeder;
use App\Models\Language;
use Carbon\Carbon;

class LanguageSeeder extends Seeder
{
    
    public function run()
    {
        $now = Carbon::now();

        $languages = [
            [
                'code' => 'en',
                'name' => 'English',
                'locale' => 'en',
                'created_at'=> $now,
                'updated_at' => $now
            ],
            [
                'code' => 'hr',
                'name' => 'Croatian',
                'locale' => 'hr',
                'created_at'=> $now,
                'updated_at' => $now
            ],
        ];

        foreach ($languages as $language) {
            Language::create($language);
        }
    }
}