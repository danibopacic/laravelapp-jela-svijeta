<?php

use Illuminate\Database\Seeder;
use App\Models\Ingredient;
use Faker\Factory as Faker;
use Carbon\Carbon;

class IngredientSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now();

        for ($i = 0; $i < 20; $i++) {
            $ingredient = Ingredient::create([
                'slug' => $faker->slug,
                'created_at' => $now,
                'updated_at' => $now,
                'deleted_at' => null
            ]);
            foreach (['en', 'hr'] as $locale) {
                $ingredient->translateOrNew($locale)->title = $faker->word;
            }
            $ingredient->save();
        }
    }
}