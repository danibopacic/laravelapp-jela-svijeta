<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;
use Faker\Factory as Faker;
use Carbon\Carbon;

class TagSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now();

        for ($i = 0; $i < 15; $i++) {
            $tag = Tag::create([
                'slug' => $faker->slug,
                'created_at' => $now,
                'updated_at' => $now,
                'deleted_at' => null
            ]);
            foreach (['en', 'hr'] as $locale) {
                $tag->translateOrNew($locale)->title = $faker->word;
            }
            $tag->save();
        }
    }
}