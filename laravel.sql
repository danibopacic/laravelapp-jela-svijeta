-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 16, 2024 at 10:11 PM
-- Server version: 8.0.36-0ubuntu0.20.04.1
-- PHP Version: 7.4.3-4ubuntu2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'corporis-porro-veniam-et-porro-quia', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(2, 'rerum-fugiat-velit-culpa-optio-quia', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(3, 'aliquid-est-nihil-est-id-illo-et-id-esse', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(4, 'doloremque-consequatur-aut-quos-ducimus-quos-et-ratione', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(5, 'voluptatem-et-nam-error-laborum-consectetur-iusto', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(6, 'ut-doloremque-autem-ratione-expedita-odit', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(7, 'sed-magni-quae-animi-iusto-et-ad', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(8, 'aut-ut-necessitatibus-quod-voluptas-assumenda-officia-qui-dolore', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(9, 'est-ea-omnis-nisi-hic', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(10, 'nisi-quo-autem-vel-optio-sed-sed', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_translations`
--

CREATE TABLE `category_translations` (
  `id` int UNSIGNED NOT NULL,
  `category_id` int UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_translations`
--

INSERT INTO `category_translations` (`id`, `category_id`, `locale`, `title`) VALUES
(1, 1, 'en', 'harum'),
(2, 1, 'hr', 'voluptas'),
(3, 2, 'en', 'dolores'),
(4, 2, 'hr', 'deleniti'),
(5, 3, 'en', 'doloremque'),
(6, 3, 'hr', 'qui'),
(7, 4, 'en', 'voluptatem'),
(8, 4, 'hr', 'perspiciatis'),
(9, 5, 'en', 'sapiente'),
(10, 5, 'hr', 'pariatur'),
(11, 6, 'en', 'saepe'),
(12, 6, 'hr', 'nisi'),
(13, 7, 'en', 'molestiae'),
(14, 7, 'hr', 'non'),
(15, 8, 'en', 'ut'),
(16, 8, 'hr', 'est'),
(17, 9, 'en', 'sed'),
(18, 9, 'hr', 'quaerat'),
(19, 10, 'en', 'quis'),
(20, 10, 'hr', 'qui');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'porro-fugit-voluptatem-quam-quibusdam-qui-unde', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(2, 'saepe-aut-nam-ad-veritatis-rerum-suscipit-maxime', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(3, 'iusto-qui-quos-voluptatem-adipisci-veniam-quo-qui', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(4, 'aut-error-optio-incidunt-quae-perspiciatis-ut', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(5, 'aliquid-unde-possimus-odit-excepturi-aliquam', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(6, 'enim-delectus-nesciunt-quia-consequatur', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(7, 'maiores-et-tempore-placeat-enim-totam-quibusdam-nulla', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(8, 'sed-quia-est-amet-et', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(9, 'accusamus-commodi-quidem-quia-nisi', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(10, 'magnam-in-delectus-ad-quia-non-animi', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(11, 'fugiat-numquam-non-ab-officiis-accusamus-praesentium-illum', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(12, 'aliquam-fugiat-odio-aperiam-praesentium-ut', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(13, 'vero-nisi-numquam-qui', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(14, 'et-est-aspernatur-nemo', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(15, 'corrupti-voluptas-aut-et-corporis-asperiores-quisquam-voluptatem', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(16, 'illo-tempore-quo-ut-magni-harum-repudiandae-nisi', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(17, 'est-autem-consequatur-cum-autem-officiis-reiciendis-minus', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(18, 'dolor-atque-dicta-et-voluptas-similique-labore', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(19, 'aut-maxime-qui-assumenda-doloremque', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL),
(20, 'et-laboriosam-neque-praesentium-qui', '2024-05-16 17:36:53', '2024-05-16 17:36:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ingredient_meal`
--

CREATE TABLE `ingredient_meal` (
  `id` int UNSIGNED NOT NULL,
  `ingredient_id` int UNSIGNED NOT NULL,
  `meal_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ingredient_meal`
--

INSERT INTO `ingredient_meal` (`id`, `ingredient_id`, `meal_id`, `created_at`, `updated_at`) VALUES
(1, 20, 1, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(2, 3, 2, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(3, 13, 2, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(4, 5, 3, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(5, 7, 3, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(6, 17, 3, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(7, 1, 4, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(8, 7, 4, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(9, 1, 5, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(10, 4, 5, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(11, 3, 6, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(12, 5, 6, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(13, 19, 6, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(14, 19, 7, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(15, 9, 8, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(16, 10, 8, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(17, 13, 8, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(18, 9, 9, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(19, 4, 10, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(20, 1, 11, NULL, NULL),
(21, 2, 11, NULL, NULL),
(22, 3, 11, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ingredient_translations`
--

CREATE TABLE `ingredient_translations` (
  `id` int UNSIGNED NOT NULL,
  `ingredient_id` int UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ingredient_translations`
--

INSERT INTO `ingredient_translations` (`id`, `ingredient_id`, `locale`, `title`) VALUES
(1, 1, 'en', 'in'),
(2, 1, 'hr', 'incidunt'),
(3, 2, 'en', 'omnis'),
(4, 2, 'hr', 'ut'),
(5, 3, 'en', 'at'),
(6, 3, 'hr', 'et'),
(7, 4, 'en', 'ipsam'),
(8, 4, 'hr', 'culpa'),
(9, 5, 'en', 'explicabo'),
(10, 5, 'hr', 'modi'),
(11, 6, 'en', 'sequi'),
(12, 6, 'hr', 'quia'),
(13, 7, 'en', 'adipisci'),
(14, 7, 'hr', 'magni'),
(15, 8, 'en', 'est'),
(16, 8, 'hr', 'dolor'),
(17, 9, 'en', 'dolore'),
(18, 9, 'hr', 'corrupti'),
(19, 10, 'en', 'asperiores'),
(20, 10, 'hr', 'aut'),
(21, 11, 'en', 'iste'),
(22, 11, 'hr', 'maiores'),
(23, 12, 'en', 'saepe'),
(24, 12, 'hr', 'et'),
(25, 13, 'en', 'non'),
(26, 13, 'hr', 'error'),
(27, 14, 'en', 'neque'),
(28, 14, 'hr', 'fuga'),
(29, 15, 'en', 'asperiores'),
(30, 15, 'hr', 'eum'),
(31, 16, 'en', 'error'),
(32, 16, 'hr', 'placeat'),
(33, 17, 'en', 'maxime'),
(34, 17, 'hr', 'accusantium'),
(35, 18, 'en', 'et'),
(36, 18, 'hr', 'maiores'),
(37, 19, 'en', 'necessitatibus'),
(38, 19, 'hr', 'numquam'),
(39, 20, 'en', 'consequatur'),
(40, 20, 'hr', 'labore');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`, `locale`, `created_at`, `updated_at`) VALUES
(1, 'en', 'English', 'en', '2024-05-16 17:36:53', '2024-05-16 17:36:53'),
(2, 'hr', 'Croatian', 'hr', '2024-05-16 17:36:53', '2024-05-16 17:36:53');

-- --------------------------------------------------------

--
-- Table structure for table `meals`
--

CREATE TABLE `meals` (
  `id` int UNSIGNED NOT NULL,
  `category_id` int UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meals`
--

INSERT INTO `meals` (`id`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(2, NULL, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(3, 10, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(4, NULL, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(5, 8, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(6, NULL, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(7, 2, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(8, NULL, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(9, 10, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(10, NULL, '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(11, 1, '2024-05-16 18:10:18', '2024-05-16 18:10:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meal_tag`
--

CREATE TABLE `meal_tag` (
  `id` int UNSIGNED NOT NULL,
  `meal_id` int UNSIGNED NOT NULL,
  `tag_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meal_tag`
--

INSERT INTO `meal_tag` (`id`, `meal_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 1, 7, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(2, 2, 11, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(3, 2, 15, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(4, 3, 5, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(5, 3, 7, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(6, 4, 14, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(7, 5, 2, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(8, 5, 10, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(9, 5, 13, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(10, 6, 2, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(11, 6, 13, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(12, 7, 2, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(13, 7, 6, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(14, 7, 8, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(15, 8, 7, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(16, 8, 13, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(17, 9, 9, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(18, 9, 11, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(19, 10, 14, '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(20, 11, 1, NULL, NULL),
(21, 11, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meal_translations`
--

CREATE TABLE `meal_translations` (
  `id` int UNSIGNED NOT NULL,
  `meal_id` int UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meal_translations`
--

INSERT INTO `meal_translations` (`id`, `meal_id`, `locale`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 'Quaerat ab tempora est voluptas eos est.', 'Magnam ipsam ut quos quaerat occaecati delectus sed. Quibusdam provident deserunt error et unde laudantium. Cumque nobis sed eaque. Asperiores est quisquam eligendi atque ut provident dolor ut.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(2, 1, 'hr', 'Rerum inventore consectetur atque rem minima possimus et.', 'Error autem dolores qui eos aut adipisci. Quisquam reiciendis commodi est dolor aut similique. Iusto ut sit consectetur quia porro. Aut non nisi sed sed facilis.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(3, 2, 'en', 'Cum eius est voluptas magni maiores ipsa.', 'Minima quia dicta expedita fugit nobis odit quaerat eveniet. Neque exercitationem ut enim saepe quia. Eos rerum quibusdam et totam veritatis doloremque libero maiores. Accusantium sequi beatae ratione et.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(4, 2, 'hr', 'Iusto fuga sint non deleniti illo placeat.', 'Amet modi est vel consequuntur qui praesentium atque. Cumque aut quisquam possimus accusamus iste nam. Asperiores et libero est nam vel repellendus aut. Dolor error natus illum culpa excepturi quia. Fugit veritatis in ratione.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(5, 3, 'en', 'Sapiente ut maxime aliquam voluptates.', 'Natus totam saepe maxime ut. Neque libero et doloribus et recusandae sit voluptas laborum.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(6, 3, 'hr', 'Numquam quasi adipisci eveniet cum dicta.', 'Non veritatis qui praesentium vel architecto aut sequi similique. Ut ut quia est. Quia quia id aut sunt magni tempore ut. Dignissimos veniam accusamus aspernatur expedita. Quod nulla expedita in aliquam.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(7, 4, 'en', 'Vero fugiat dolores harum ea et perferendis.', 'Fugit vitae minima consequatur necessitatibus amet id commodi. Consequatur est perspiciatis beatae. Quia repudiandae est necessitatibus tenetur et.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(8, 4, 'hr', 'Qui cupiditate occaecati doloribus a dolores.', 'Aspernatur magnam ut molestiae incidunt nesciunt rerum. Soluta omnis cumque sed est aut accusamus odit.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(9, 5, 'en', 'Temporibus esse quia officia voluptas voluptatem.', 'Vero in quas quaerat. Rerum cum autem quasi fuga. Error facilis eveniet aut aliquid non velit sunt.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(10, 5, 'hr', 'Occaecati consequatur soluta beatae incidunt.', 'Laudantium et quaerat totam eum est occaecati. Inventore blanditiis officiis odit tempora. Harum autem corporis velit dolor consequatur aut.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(11, 6, 'en', 'Neque delectus incidunt sed.', 'Qui soluta sint numquam eos. Ducimus facilis ea numquam sint similique sint deserunt sit.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(12, 6, 'hr', 'Velit quasi est consectetur quod quia.', 'Occaecati ut harum debitis non fuga. Qui assumenda reiciendis est quasi et.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(13, 7, 'en', 'Minus labore dicta placeat quae ut corrupti temporibus.', 'Adipisci consectetur sit at laudantium exercitationem quo qui. Qui ratione quos in error. Cum eligendi impedit vel. Vel quaerat vel molestiae.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(14, 7, 'hr', 'Et eum doloremque odit laboriosam.', 'Eum et unde dignissimos facere laudantium officia. Atque ducimus saepe aut asperiores nulla eum. Totam ipsa ut vel sequi.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(15, 8, 'en', 'Cum incidunt ab nihil labore nobis aut velit.', 'Dolor blanditiis fugiat ut et similique eius aut. Voluptatum aut assumenda totam recusandae soluta. Voluptas quis voluptatem voluptates quae quisquam in recusandae culpa.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(16, 8, 'hr', 'Et eum ut qui dignissimos voluptatibus eaque nihil.', 'Consequuntur possimus aspernatur quidem corrupti velit eligendi. Ut quas voluptatibus exercitationem qui pariatur veritatis sed. Quos sunt sit repudiandae natus est fugiat eos.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(17, 9, 'en', 'Quasi eos occaecati aut accusantium quidem quidem.', 'Provident non provident alias rerum nostrum molestiae voluptatem. Consequuntur velit mollitia incidunt saepe ea magnam dicta nobis. Nulla nostrum est non nulla.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(18, 9, 'hr', 'Nihil sint reiciendis asperiores dolore est facilis.', 'Hic doloribus voluptate maiores commodi. Voluptate quas officiis voluptates dolore explicabo totam tenetur. Et iure expedita aut quisquam dolorum in. Ut quos ut et quis.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(19, 10, 'en', 'Eius nesciunt enim ex ex omnis.', 'Esse aspernatur qui facilis quia iusto autem. Officiis quidem aut non debitis. Dignissimos laboriosam enim cupiditate illo vel dolores voluptatem. Sed et quasi saepe exercitationem atque doloribus.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(20, 10, 'hr', 'Consequuntur quis illo itaque labore laudantium dolore.', 'Velit repellat quidem ducimus eos sequi tempora aliquid. Itaque beatae qui explicabo soluta consequatur exercitationem maiores id.', '2024-05-16 17:36:54', '2024-05-16 17:36:54'),
(21, 11, 'en', 'Test Meal', 'This is a test meal.', '2024-05-16 18:10:18', '2024-05-16 18:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(74, '2024_05_16_175950_create_languages_table', 1),
(75, '2024_05_16_180216_create_ingredients_table', 1),
(76, '2024_05_16_180307_create_categories_table', 1),
(77, '2024_05_16_180353_create_tags_table', 1),
(78, '2024_05_16_184031_create_meals_table', 1),
(79, '2024_05_16_190853_create_meal_translations_table', 1),
(80, '2024_05_16_192941_create_ingredient_meal_table', 1),
(81, '2024_05_16_192953_create_meal_tag_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'eum-et-et-enim-consequatur-magni-et-iure', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(2, 'voluptates-voluptas-voluptas-sint-nihil-vel-quaerat', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(3, 'expedita-earum-non-quis-exercitationem-ut-ut-maxime', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(4, 'non-non-esse-sunt-inventore-sint-quas-quam-veritatis', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(5, 'cupiditate-assumenda-et-consectetur-eveniet', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(6, 'quis-earum-consectetur-quidem-non-ea', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(7, 'porro-ut-ut-et-veniam', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(8, 'cupiditate-tenetur-dolores-atque', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(9, 'quidem-voluptas-enim-laborum-quo', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(10, 'ut-distinctio-incidunt-eaque-earum-illo-rerum', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(11, 'doloribus-quis-dolore-nihil-ipsum-magni-necessitatibus-ut', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(12, 'non-natus-porro-nemo-quia', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(13, 'ipsam-quidem-adipisci-maxime-deserunt-ipsam-beatae-unde-et', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(14, 'corporis-culpa-ipsum-dolorum-et-eaque', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL),
(15, 'impedit-mollitia-aut-laborum-voluptas-odit-minima-non-sed', '2024-05-16 17:36:54', '2024-05-16 17:36:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tag_translations`
--

CREATE TABLE `tag_translations` (
  `id` int UNSIGNED NOT NULL,
  `tag_id` int UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag_translations`
--

INSERT INTO `tag_translations` (`id`, `tag_id`, `locale`, `title`) VALUES
(1, 1, 'en', 'reiciendis'),
(2, 1, 'hr', 'et'),
(3, 2, 'en', 'est'),
(4, 2, 'hr', 'in'),
(5, 3, 'en', 'cumque'),
(6, 3, 'hr', 'ratione'),
(7, 4, 'en', 'veniam'),
(8, 4, 'hr', 'excepturi'),
(9, 5, 'en', 'nostrum'),
(10, 5, 'hr', 'voluptatem'),
(11, 6, 'en', 'ipsam'),
(12, 6, 'hr', 'ea'),
(13, 7, 'en', 'corrupti'),
(14, 7, 'hr', 'beatae'),
(15, 8, 'en', 'eos'),
(16, 8, 'hr', 'vero'),
(17, 9, 'en', 'cumque'),
(18, 9, 'hr', 'temporibus'),
(19, 10, 'en', 'necessitatibus'),
(20, 10, 'hr', 'debitis'),
(21, 11, 'en', 'sapiente'),
(22, 11, 'hr', 'cupiditate'),
(23, 12, 'en', 'et'),
(24, 12, 'hr', 'qui'),
(25, 13, 'en', 'ut'),
(26, 13, 'hr', 'quia'),
(27, 14, 'en', 'aut'),
(28, 14, 'hr', 'deleniti'),
(29, 15, 'en', 'commodi'),
(30, 15, 'hr', 'sit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_translations_category_id_locale_unique` (`category_id`,`locale`),
  ADD KEY `category_translations_locale_index` (`locale`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ingredients_slug_unique` (`slug`);

--
-- Indexes for table `ingredient_meal`
--
ALTER TABLE `ingredient_meal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ingredient_meal_ingredient_id_foreign` (`ingredient_id`),
  ADD KEY `ingredient_meal_meal_id_foreign` (`meal_id`);

--
-- Indexes for table `ingredient_translations`
--
ALTER TABLE `ingredient_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ingredient_translations_ingredient_id_locale_unique` (`ingredient_id`,`locale`),
  ADD KEY `ingredient_translations_locale_index` (`locale`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `languages_code_unique` (`code`),
  ADD UNIQUE KEY `languages_locale_unique` (`locale`);

--
-- Indexes for table `meals`
--
ALTER TABLE `meals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meals_category_id_foreign` (`category_id`);

--
-- Indexes for table `meal_tag`
--
ALTER TABLE `meal_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meal_tag_meal_id_foreign` (`meal_id`),
  ADD KEY `meal_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `meal_translations`
--
ALTER TABLE `meal_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `meal_translations_meal_id_locale_unique` (`meal_id`,`locale`),
  ADD KEY `meal_translations_locale_index` (`locale`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `tag_translations`
--
ALTER TABLE `tag_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_translations_tag_id_locale_unique` (`tag_id`,`locale`),
  ADD KEY `tag_translations_locale_index` (`locale`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `category_translations`
--
ALTER TABLE `category_translations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ingredient_meal`
--
ALTER TABLE `ingredient_meal`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ingredient_translations`
--
ALTER TABLE `ingredient_translations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meals`
--
ALTER TABLE `meals`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `meal_tag`
--
ALTER TABLE `meal_tag`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `meal_translations`
--
ALTER TABLE `meal_translations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tag_translations`
--
ALTER TABLE `tag_translations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ingredient_meal`
--
ALTER TABLE `ingredient_meal`
  ADD CONSTRAINT `ingredient_meal_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ingredient_meal_meal_id_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meals` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ingredient_translations`
--
ALTER TABLE `ingredient_translations`
  ADD CONSTRAINT `ingredient_translations_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `meals`
--
ALTER TABLE `meals`
  ADD CONSTRAINT `meals_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `meal_tag`
--
ALTER TABLE `meal_tag`
  ADD CONSTRAINT `meal_tag_meal_id_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meals` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `meal_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `meal_translations`
--
ALTER TABLE `meal_translations`
  ADD CONSTRAINT `meal_translations_meal_id_foreign` FOREIGN KEY (`meal_id`) REFERENCES `meals` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tag_translations`
--
ALTER TABLE `tag_translations`
  ADD CONSTRAINT `tag_translations_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
